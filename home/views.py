
# -- VIEWS ------------------------------------------------------------------- #

from django.views import generic
from django.shortcuts import render, redirect

## HOME: Vultures
class Home(generic.View):
    template_name = "home/home.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = { "Universidad" : "Universidad Tecnológica de Tijuana",
            "Empresa" : "Vultures Academy", "Desarrolladora" : "Vultures",
        }
        return render(request, self.template_name, self.context)

# ---------------------------------------------------------------------------- #